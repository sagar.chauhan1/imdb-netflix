from django.db import models
from django.contrib.auth.models import User
from django.conf import settings


class Movie(models.Model):
    title = models.CharField(max_length=200, blank=False)
    movie_id = models.IntegerField(default=0, primary_key=True)
    poster_link = models.CharField(max_length=200)
    trailer_link = models.CharField(max_length=200)
    genre = models.CharField(max_length=300)
    popularity = models.FloatField()
    video_link = models.CharField(max_length=300)


class Preference(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    preference_one = models.CharField(max_length=100)
    preference_two = models.CharField(max_length=100)
    preference_three = models.CharField(max_length=100)


class Watchlist(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    movie_id = models.ForeignKey(Movie, on_delete=models.CASCADE)


class Category(models.Model):
    name = models.CharField(max_length=200, unique=True)
    movie_id = models.ForeignKey(Movie, on_delete=models.CASCADE)


class Upload(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    movie_id = models.IntegerField(unique=True)
    title = models.CharField(max_length=200, blank=False)
    info = models.TextField()
    poster_link = models.URLField()
    trailer_link = models.URLField(null=True)
    genre = models.CharField(max_length=300)
